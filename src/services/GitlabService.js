import Api from '@/services/Api'

const BASE_URL = 'https://lab.ssafy.com/api/v4'
const TEST_URL = 'https://gitlab.com/api/v4'

export default {
	getRepos(projectID) {
		// return Api(TEST_URL).get(`/users/hackurity01/projects`)
		return Api(TEST_URL).get(`/projects/${projectID}`)
	},
	getCommits(fullName) {
		let d = new Date()
		d.setMonth(d.getMonth() - 1)

		return Api(TEST_URL).get(`/projects/${fullName}/repository/commits?since=${d.toISOString()}`)
	},
	getMembers(projectID) {
		return Api(TEST_URL).get(`/projects/${projectID}/members`)
	}
}